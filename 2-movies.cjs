const favouritesMovies = {
    "Matrix": {
        imdbRating: 8.3,
        actors: ["Keanu Reeves", "Carrie-Anniee"],
        oscarNominations: 2,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$680M"
    },
    "FightClub": {
        imdbRating: 8.8,
        actors: ["Edward Norton", "Brad Pitt"],
        oscarNominations: 6,
        genre: ["thriller", "drama"],
        totalEarnings: "$350M"
    },
    "Inception": {
        imdbRating: 8.3,
        actors: ["Tom Hardy", "Leonardo Dicaprio"],
        oscarNominations: 12,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$870M"
    },
    "The Dark Knight": {
        imdbRating: 8.9,
        actors: ["Christian Bale", "Heath Ledger"],
        oscarNominations: 12,
        genre: ["thriller"],
        totalEarnings: "$744M"
    },
    "Pulp Fiction": {
        imdbRating: 8.3,
        actors: ["Sameul L. Jackson", "Bruce Willis"],
        oscarNominations: 7,
        genre: ["drama", "crime"],
        totalEarnings: "$455M"
    },
    "Titanic": {
        imdbRating: 8.3,
        actors: ["Leonardo Dicaprio", "Kate Winslet"],
        oscarNominations: 13,
        genre: ["drama"],
        totalEarnings: "$800M"
    }
}


/*
    NOTE: For all questions, the returned data must contain all the movie information including its name.

 
    Q2. Find all the movies who got more than 3 oscarNominations and also totalEarning are more than $500M.
    Q.3 Find all movies of the actor "Leonardo Dicaprio".
    Q.4 Sort movies (based on IMDB rating)
        if IMDB ratings are same, compare totalEarning as the secondary metric.
    Q.5 Group movies based on genre. Priority of genres in case of multiple genres present are:
        drama > sci-fi > adventure > thriller > crime

    NOTE: Do not change the name of this file
*/
function applyFromEntries(array) {
    return Object.fromEntries(array)
}

const arraysOfMovies = Object.entries(favouritesMovies)

const moviesWithTotalEarningsMoreThan$500M = arraysOfMovies.filter(movie => {
    let earnings = +movie[1].totalEarnings.match(/\d+/g)
    return 500 < earnings
})

// console.log(applyFromEntries(moviesWithTotalEarningsMoreThan$500M))


const moreThanThreeOscarNominationsAndTotalEarningMoreThan500M = moviesWithTotalEarningsMoreThan$500M.filter(movie => {

    return movie[1].oscarNominations > 3
})
// console.log(applyFromEntries(moreThanThreeOscarNominationsAndTotalEarningMoreThan500M))

const moviesOfActorLeonardoDicaprio = arraysOfMovies.filter(movie => {

    return movie[1].actors.includes("Leonardo Dicaprio")
})
// console.log(applyFromEntries(moviesOfActorLeonardoDicaprio))


const sortMovies = arraysOfMovies.sort((movie1, movie2) => {

    const earning1 = +movie1[1].totalEarnings.match(/\d+/g)
    const earning2 = +movie2[1].totalEarnings.match(/\d+/g)

    return earning2 - earning1
})
    .sort((movie1, movie2) => {

        return movie2[1].imdbRating - movie1[1].imdbRating
    })

// console.log(applyFromEntries(sortMovies))

const groupMoviesBasedOnGenre = Object.keys(favouritesMovies)
    .map(movie => {

        let genres = favouritesMovies[movie].genre

        if (genres.includes("drama")) {
            return ['drama', movie, favouritesMovies[movie]]
        }
        else if (genres.includes("sci-fi")) {
            return ['sci-fi', movie, favouritesMovies[movie]]
        }
        else if (genres.includes("adventure")) {
            return ['adventure', movie, favouritesMovies[movie]]
        }
        else if (genres.includes("thriller")) {
            return ['thriller', movie, favouritesMovies[movie]]
        }
        else {
            return ['crime', movie, favouritesMovies[movie]]

        }
    })
    .reduce((output, movie) => {

        output[movie[0]].push(applyFromEntries([[movie[1], movie[2]]]))
        return output
    }, {
        drama: [],
        'sci-fi': [],
        adventure: [],
        thriller: [],
        crime: []
    })
// console.log(groupMoviesBasedOnGenre);




//below code done using functions

// function moviesWithTotalEarningsMoreThan$500M(favouritesMovies) {
//     const result = Object.entries(favouritesMovies)
//         .filter(movie => {
//             let earnings = +movie[1].totalEarnings.match(/\d+/g)
//             return 500 < earnings
//         })
//         .reduce((output, movie) => {

//             output[movie[0]] = movie[1]
//             return output
//         }, {})
//     return result
// }
// let moviesWithMoreThen500MEarnings = moviesWithTotalEarningsMoreThan$500M(favouritesMovies)

// // // console.log(moviesWithMoreThen500MEarnings)


// // function moreThan3OscarNominationsAndTotalEarningMoreThan$500M(moviesWithMoreThen500MEarnings) {
// //     const result = Object.entries(moviesWithMoreThen500MEarnings).filter(movie => {

// //         return movie[1].oscarNominations > 3
// //     })
// //         .reduce((output, movie) => {

// //             output[movie[0]] = movie[1]
// //             return output
// //         }, {})

// //     return result
// // }

// // // console.log(moreThan3OscarNominationsAndTotalEarningMoreThan$500M(moviesWithMoreThen500MEarnings))

// // function moviesOfActorLeonardoDicaprio(favouritesMovies) {
// //     const result = Object.entries(favouritesMovies).filter(movie => {

// //         return movie[1].actors.includes("Leonardo Dicaprio")
// //     })
// //         .reduce((output, movie) => {

// //             output[movie[0]] = movie[1]
// //             return output
// //         }, {})
// //     return result
// // }

// // // console.log(moviesOfActorLeonardoDicaprio(favouritesMovies))

// // function sortMovies(favouritesMovies) {
// //     const result = Object.entries(favouritesMovies).sort((movie1, movie2) => {

// //         const earning1 = +movie1[1].totalEarnings.match(/\d+/g)
// //         const earning2 = +movie2[1].totalEarnings.match(/\d+/g)

// //         return earning2 - earning1
// //     })
// //         .sort((movie1, movie2) => {

// //             return movie2[1].imdbRating - movie1[1].imdbRating
// //         })

// //     return result
// // }
// // // console.log( sortMovies(favouritesMovies))

// // //drama > sci-fi > adventure > thriller > crime

// // function groupMoviesBasedOnGenre(favouritesMovies) {
// //     let result = {
// //         drama: [],
// //         scifi: [],
// //         adventure: [],
// //         thriller: [],
// //         crime: []
// //     }
// //     result = Object.entries(favouritesMovies).reduce((output, movie) => {
// //         let genres = movie[1].genre

// //         let movieData = {}
// //         if (genres.includes("drama")) {
// //             movieData[movie[0]] = movie[1]
// //             output['drama'].push(movieData)
// //         }
// //         else if (genres.includes("sci-fi")) {
// //             movieData[movie[0]] = movie[1]
// //             output['scifi'].push(movieData)

// //         }
// //         else if (genres.includes("adventure")) {
// //             movieData[movie[0]] = movie[1]
// //             output['adventure'].push(movieData)

// //         }
// //         else if (genres.includes("thriller")) {
// //             movieData[movie[0]] = movie[1]
// //             output['thriller'].push(movieData)

// //         }
// //         else {
// //             movieData[movie[0]] = movie[1]
// //             output['crime'].push(movieData)

// //         }

// //         return output

// //     }, result)
// //     return result

// // }
// // console.log(groupMoviesBasedOnGenre(favouritesMovies))